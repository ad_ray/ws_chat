import time
import argparse
import html 

import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web


SERVER = "localhost"
PORT   = 8080

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        loader = tornado.template.Loader(".")
        self.write(loader.load("index.html").generate(server=SERVER,
                                                      port=PORT))
 

class WSHandler(tornado.websocket.WebSocketHandler):
    
    connections = set()

    def open(self):
        print('new connection')
        WSHandler.connections.add(self)

    def broadcast(self, message):
        for conn in WSHandler.connections:
            conn.write_message(message)
      
    def on_message(self, message):
        print('message received {}'.format(message));
        self.broadcast('[{}]{}'.format(time.strftime('%H:%M:%S'),
                                       html.escape(message)))
 
    def on_close(self):
      print('connection closed')
      WSHandler.connections.remove(self)
 
 
application = tornado.web.Application([
    (r'/ws', WSHandler),
    (r'/', MainHandler),
])
 
 
if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument('-s', '--server', type=str, 
                    dest='server', metavar='SERVER', default=SERVER)
    ap.add_argument('-p', '--port', type=int, 
                    dest='port', metavar='PORT', default=PORT)
    args = ap.parse_args()
    SERVER = args.server
    PORT = args.port

    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(PORT)
    tornado.ioloop.IOLoop.instance().start()
    
